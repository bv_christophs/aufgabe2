#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "BinBV.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <string>


void BinMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	
	char eingabe = '0';
	bool exit = false;
	
	do {
		int anz = 0;
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Shrink\n");

		printf("2 = Blow\n");
	
		printf("3 = Pixel zaelen\n");

		printf("4 = Oeffnen\n");

		printf("5 = Schkliessen\n");

		printf("6 = Grassfire\n");

		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);

		printf("\n");

		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'1':
			shrinkImmage(inImg, outImg);
			writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
			//viewImage_ppm();
			break;
		case '2':
			blowImmage(inImg,outImg);
			writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
			///viewImage_ppm();
			break;
		case '3':
			pixelCount = countWhitePixel(inImg);
			system("cls");
			printf("====================PIXEL Zaelen============================\n");
			printf("Die nicht schwartzen Pixel wuren Gezaelt. \nEs wurden %d nicht schwartzen Pixel gefunden\n", countWhitePixel(inImg));
			printf("============================================================\n");
			printf("Vorgang abgeschlossen!!! \nBitte dueken sie eine belibige taste um in das Menue zurueckzugelangen\n");
			_getch();
			break;

		case '4':
			system("cls");
			printf("====================OEFFNEN============================\n");
			printf("Wie oft soll Dilatiert BZW Erodiert werden ? (Bitte ganzzahl eingeben) :");
			scanf("%i",&anz);
			if (anz > 0)
			{
				oeffnen(inImg, outImg, anz);
				writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
				printf("=======================================================\n");
				//printf("vorgang abgeschlossen!!! \nBitte dueken sie eine belibige taste um in das Menue zurueckzugelangen\n");
				//_getch();
			}
			else
			{
				system("cls");
				printf("Eine Falsche eingabe wurde erkannt!!! \n Bitte geben sie eine Ganzahl eine welche groe�er als 0 ist\nDruecken sie eine belibige taste um in das menue zurueckzugehren ");
				_getch();
			}

			
			
			break;
		case '5':
			system("cls");
			printf("====================Schliessen============================\n");
			printf("Wie oft soll Dilatiert BZW Erodiert werden ? (Bitte ganzzahl eingeben) :");
			scanf("%i", &anz);

			if (anz > 0)
			{
				schliessen(inImg, outImg, anz);
				writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
				printf("=======================================================\n");
			}
			else
			{
				system("cls");
				printf("Eine Falsche eingabe wurde erkannt!!! \n Bitte geben sie eine Ganzahl eine welche groe�er als 0 ist\nDruecken sie eine belibige taste um in das menue zurueckzugehren ");
				_getch();
			}
			break;

		case '6':
			system("cls");
			printf("====================Grassfire============================\n");
			
			anz =grassfire_for_OBJcounting(inImg);
			printf("Erfolg !!! es wurden %i Objekte gefunden\n",anz);
			printf("=======================================================\n");
			printf("Vorgang abgeschlossen!!! \nBitte dueken sie eine belibige taste um in das Menue zurueckzugelangen\n");
			_getch();
			break;



		default:
			printf("Sie haben eine falsche eingabe getaetigt\n");
			break;
		}




		
	} while (exit == false);


}

void blowImmage(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM])
{


	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			if (INimg[x][y] == 255)
			{
				OUTimg[x][y] = 255;
				OUTimg[x][y - 1] = 255;
				OUTimg[x][y + 1] = 255;
				OUTimg[x - 1][y] = 255;
				OUTimg[x + 1][y] = 255;

			}
			else
			{


			}

		}
	}


}


void shrinkImmage(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM])
{



	for (int x = 1; x < MAXXDIM - 1; x++)
	{
		for (int y = 1; y < MAXYDIM - 1; y++)
		{
			if (INimg[x][y] == 255 && INimg[x - 1][y] == 255 && INimg[x + 1][y] == 255 && INimg[x][y - 1] == 255 && INimg[x][y + 1] == 255)
			{
				OUTimg[x][y] = 255;


			}
			else
			{

				OUTimg[x][y] = 0;
			}

		}
	}
}
// ===============================================Pixel count Funktion ============================================

// Die Funktion bestimmt die anzahl der nicht weissen pixel 
// Als eingabe dient ein Bild mit 256 x 256 Pixeln die ruekgabe ist eine ganzzahl.
int countWhitePixel(unsigned char img[MAXXDIM][MAXYDIM])
{
	int erg = 0;

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			if(img[x][y] != 0)
			{
				erg++;
			}
		}
	}
	return erg;
}
// ===============================================Oeffnen Funktion ============================================
// Die funktion fuert den Oeffnen Algorythmus aus
// Als eingabe dient ein Bild mit 256 x 256 Pixeln und die anzahl der durchlaufe sowie ein platzhalter fuer das ausgabe Immage


void oeffnen(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int wieoft)
{
	unsigned char slot1[MAXXDIM][MAXYDIM];

	unsigned char slot2[MAXXDIM][MAXYDIM];

	initImmage(slot1);
	initImmage(slot2);

	copyImmage(inImg, slot1);
	for (int count = 0; count < wieoft;count++)
	{
		shrinkImmage(slot1, slot2);
		copyImmage(slot2, slot1);

	}

	for (int count = 0; count < wieoft;count++)
	{
		
		blowImmage(slot1, slot2);
		copyImmage(slot2, slot1);

	}
	copyImmage(slot1, outImg);

}


// ===============================================Schlei�en Funktion ============================================
// Die funktion fuert den Oeffnen Algorythmus aus
// Als eingabe dient ein Bild mit 256 x 256 Pixeln und die anzahl der durchlaufe sowie ein platzhalter fuer das ausgabe Immage

void schliessen(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int wieoft)
{
	unsigned char slot1[MAXXDIM][MAXYDIM];

	unsigned char slot2[MAXXDIM][MAXYDIM];

	initImmage(slot1);
	initImmage(slot2);

	copyImmage(inImg, slot1);

	for (int count = 0; count < wieoft;count++)
	{
		blowImmage(slot1, slot2);
		copyImmage(slot2, slot1);

	}

	for (int count = 0; count < wieoft;count++)
	{

		
		shrinkImmage(slot1, slot2);
		copyImmage(slot2, slot1);

	}
	copyImmage(slot1, outImg);

}

// ===============================================Grassfire  Funktion ============================================
// Die funktion fuert den Grassfire Algorythmus aus
// Als eingabe dient ein Bild mit 256 x 256 Pixeln
// die rueckgabe ist die anzahl der gefundennen Objekte.

int grassfire_for_OBJcounting(unsigned char inImg[MAXXDIM][MAXYDIM])
{
	// Alle Konstanten sind in der BinBV.h Definiert !!! und kommentiert 
	bool objCountGrowing = true;
	int objCounter = 0;
	int oldObjCounter = 0;



	
	unsigned char markerImg[MAXXDIM][MAXYDIM];
	unsigned char tempImg[MAXXDIM][MAXYDIM];

	// alles Null schreiben
	initImmage(markerImg);
	initImmage(tempImg);

	// Zuendpunkt suchen Hier while schleife weil diese veralssen werden soll wenn der punkt gefunden wurde!
	
	while (objCountGrowing)
	//solange die anzahl der objekte zunimmt...
	{
		int zp[2] = { 0,0 };
		bool zpFound = false;

		int zpx = 1;
		int zpy = 1;
	// zuendpunkt suchen ... Wenn gefunden Abrch der schleife 
	while (zpx < MAXXDIM - 1 && zpFound == false) 
	{
		zpy = 1;
		while (zpy < MAXYDIM - 1 && zpFound == false)
		{ 
			
			if (inImg[zpx][zpy] >= GF_TRIGGERLEVEL && tempImg[zpx][zpy] == GF_NOTUSED)
			{ 
				//Der zuendpunkt mus weiss sein und noch nie bearbeitet worden sein 
			zp[0] = zpx;
			zp[1] = zpy;
			zpFound = true;
			}
			
			zpy++;
		}
		zpx++;
	}
	
	bool objGrowing = true;
	int oldPixelCount = 0;
	int newPixelCount = 0;

	if (zpFound == true)
	{
		//wenn ein zuenpunkt gefunden wurde.
		//zuendpunkt im Markierungs Bild setzen 
		markerImg[zp[0]][zp[1]] = GF_WHITE;
		// das Pixel im Temp  Bild auf in process setzen
		tempImg[zp[0]][zp[1]] = GF_INPROCESS;

		while (objGrowing == true) // eigendlicher Grasfire 
			//solange die das ojekt groe�er wird ..
		{
			
			for (int x = 1; x < MAXXDIM - 1; x++)
			{
				
				for (int y = 1; y < MAXYDIM - 1; y++)
				{
					
					if (markerImg[x][y] >= GF_TRIGGERLEVEL && tempImg[x][y] == GF_INPROCESS)
					{
						// Delatiere mit elementarraute 
						markerImg[x][y] = GF_WHITE;
						markerImg[x + 1][y] = GF_WHITE;
						markerImg[x - 1][y] = GF_WHITE;
						markerImg[x][y + 1] = GF_WHITE;
						markerImg[x][y - 1] = GF_WHITE;



						//und Verknuepfung ... Original und markierungsbild 
						if (!(inImg[x + 1][y] >= GF_TRIGGERLEVEL &&markerImg[x][y + 1] >= GF_WHITE))
						{
							// Pixel zurueksetzen 
							markerImg[x + 1][y] = GF_BLACK;
						}
						else
						{
							if (tempImg[x + 1][y] != GF_DONE)
							{
								//wenn nicht schon abgearbeitet dann in Process setzen.
								tempImg[x + 1][y] = GF_INPROCESS;
							}
						}
						//und Verknuepfung ... Original und markierungsbild 
						if (!(inImg[x - 1][y] >= GF_TRIGGERLEVEL &&markerImg[x][y - 1] >= GF_WHITE))
						{
							// Pixel zurueksetzen 
							markerImg[x - 1][y] = GF_BLACK;
						}
						else
						{
							if (tempImg[x - 1][y] != GF_DONE)
							{
								//wenn nicht schon abgearbeitet dann in Process setzen.
								tempImg[x - 1][y] = GF_INPROCESS;
							}
						}
						//und Verknuepfung ... Original und markierungsbild 
						if (!(inImg[x][y + 1] >= GF_TRIGGERLEVEL &&markerImg[x][y + 1] >= GF_WHITE))
						{
							// Pixel zurueksetzen 
							markerImg[x][y + 1] = GF_BLACK;
						}
						else
						{
							if (tempImg[x][y + 1] != GF_DONE)
							{
								//wenn nicht schon abgearbeitet dann in Process setzen.
								tempImg[x][y + 1] = GF_INPROCESS;
							}
						}
						//und Verknuepfung ... Original und markierungsbild 
						if (!(inImg[x][y - 1] >= GF_TRIGGERLEVEL &&markerImg[x][y - 1] >= GF_WHITE))
						{
							// Pixel zurueksetzen 
							markerImg[x][y - 1] = GF_BLACK;
						}
						else
						{
							if (tempImg[x][y - 1] != GF_DONE)
							{
								//wenn nicht schon abgearbeitet dann in Process setzen.
								tempImg[x][y - 1] = GF_INPROCESS;
							}
						}

						//und Verknuepfung ... Original und markierungsbild 
						if (!(inImg[x][y] >= GF_TRIGGERLEVEL &&markerImg[x][y] >= GF_WHITE))
						{
							// Pixel zurueksetzen 
							markerImg[x][y] = GF_BLACK;
						}
						else
						{
							// pixel abgearbeitet
							tempImg[x][y] = GF_DONE;
						}

					}

				}
			}
			// nicht schwatze pixel zaehlen ..
			newPixelCount = countWhitePixel(markerImg);

			if (newPixelCount > oldPixelCount)
			{
				// wenn die anzahl zugenommen hat sclaeife neusterten 
				objGrowing = true;
			}
			else
			{

				// wenn die anzahl nicht zunimmt dann Objek abgeschlossen 
				objGrowing = false;
				//und objektcounter hochzaehlen
				objCounter++;
				//My_IMGDEBUG(markerImg);
			}

			oldPixelCount = newPixelCount;

		} 
		
		

	}// Grasfeuer
	
	// ueberpruefen ob die objektanzahl zunimmt ...
	if (objCounter>oldObjCounter)
	{
		// im Algorytmus bleiben.
		objCountGrowing = true;
		

	}
	else
	{
		// Algorytmus beenden
		objCountGrowing = false;

	}


	oldObjCounter = objCounter;


	}//objektschleife 

	
	return objCounter;
}